package com.donespeak.visofh.sometest;

import java.text.Normalizer;
import java.text.Normalizer.Form;

import org.junit.Test;

import junit.framework.TestCase;

public class TextUtilTest extends TestCase {
	
	public void normalizedTest() {
		System.out.println("One Hanzi: " + "我".length() + ", bytes: " + "我".getBytes().length);
		String input = "我8";
		byte[] inputByte = input.getBytes();
		Form[] forms = Form.values();
		for(Form form: forms) {
			String output = Normalizer.normalize(input, form);
			System.out.println("Form: " + form.name());
			System.out.println("Output:[" + output + "]");
			System.out.println("input length: " + input.length() + ", output length: " + output.length());
			System.out.println("inputByte: " + inputByte.length + ", outputByte: " + output.getBytes().length);
			System.out.println("========================");
		}
		String str = "一二三123abc";
		for(int i = 0; i < str.length(); i ++) {
			System.out.println(str.charAt(i));
		}
		System.out.println("=====================");
		System.out.println(str.substring(1, 2));
		System.out.println("d;；".getBytes().length);
		System.out.println("我".getBytes().length);
	}
	
	@Test
	public void testCharacterJudge(){
		// Test 实例需要以 test 开始，且全部小写
		try{
			char[] cs = new char[]{'我', 'w', '1'};
			for(char ch: cs) {
				System.out.println(ch + " is digit: " + Character.isDigit(ch));
				System.out.println(ch + " is letter: " + Character.isLetter(ch));
				System.out.println(ch + " is isAlphabetic: " + Character.isAlphabetic(ch));
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("输出");
		}
		System.out.println("space: " + Character.isWhitespace('\n'));
	}
}
