package com.donespeak.visofh.analysis;

import java.util.List;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * 
 * @author guanrong.yang
 * @date 2018/03/13
 */
public class TestNGram extends TestCase{

	@Test
	public void testSpiltAndHighLight() {
		String input = "我可笑沉溺醉意自囚 拼却全力弃置翻覆权谋 梦中的你眉目依旧 缓缓步入万家灯火烧(Hate!) Hunt me down like the C.I.A 2018-3-13";
		NGram nGram = new NGram(2);
		List<Token> tokens = nGram.split(input);
		for(Token token: tokens) {
			System.out.println(token);
		}
		System.out.println(highLight("眉目", input, tokens));
	}
	
	public String highLight(String word, String text, List<Token> tokens){
		Token p = null;
		for(Token token: tokens) {
			if(token.value.equals(word)) {
				p = token;
			}
		}
		if(p == null) {
			return text;
		}
		return text.substring(0, p.position) + "<span class='highlight'>" + 
		text.substring(p.position, p.position + p.value.length()) + "</span>" + text.substring(p.position + p.value.length());
	}
}
