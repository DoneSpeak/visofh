package com.donespeak.visofh.analysis;

public class Token {
	public String value;
	public int position;
	
	public Token(String value, int position) {
		this.value = value;
		this.position = position;
	}

	@Override
	public String toString() {
		return "Token [value=" + value + ", position=" + position + "]";
	}
}
