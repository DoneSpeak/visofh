package com.donespeak.visofh.analysis;

import java.util.ArrayList;
import java.util.List;

import com.donespeak.visofh.util.CharacterUtil;

public class NGram {
	private int n;
	private int skipStep = 1;

	public NGram(int n){
		this.n = n;
	}
	
	public Token next(){
		return null;
	}
	
	public List<Token> split(String text){
		List<Token> tokens = new ArrayList<Token>();
		if(text == null || text.length() == 0){
			return tokens;
		}
		int length = text.length();
		for(int i = 0; i < length;) {
			// skip all ignored char
			for(;i < length && isIgnoredChar(text.charAt(i)); i ++){}
			if(i < length) {
				if(CharacterUtil.isAlphaOrDigit(text.charAt(i))) {
					i = getAlphaOrNumberWord(i, text, tokens);
				} else {
					i = getChineseWord(i, text, tokens);
				}
			}
		}
		return tokens;
	}
	
	public boolean isIgnoredChar(char ch){
		return CharacterUtil.isPunctuation(ch);
	}
	
	public int getAlphaOrNumberWord(int i, String text, List<Token> tokens){
		int start = i;
		for(;i < text.length() && CharacterUtil.isAlphaOrDigit(text.charAt(i)); i ++){}
		String word = text.substring(start, i);
		tokens.add(new Token(word, start));
		return i;
	}
	
	public int getChineseWord(int i, String text, List<Token> tokens){
		int start = i;
		for(;i < text.length() && i - start < n && !isIgnoredChar(text.charAt(i)); i ++){}
		String word = "";
		if(i - start == n) {
			word = text.substring(start, i);
		} else {
			start = i - n > 0? i - n: 0;
			word = text.substring(start, i);
		}
		tokens.add(new Token(word, start));
		return start + 1 + skipStep;		
	}
}
