package com.donespeak.visofh.util;

public class CharacterUtil {

	
	public static boolean isPunctuation(char ch) {
		return isBlankPunction(ch) || isEnglishPunctuation(ch) || isChinesePunctuation(ch);
	}
	
	public static boolean isBlankPunction(char ch) {
		return Character.isWhitespace(ch);
	}
	
	/**
	 * @reference [ASCII](https://zh.wikipedia.org/wiki/ASCII)
	 * @param ch
	 * @return
	 */
	public static boolean isEnglishPunctuation(char ch){
		switch(ch) {
		case '!': case '"': case '#': case '$': case '%': case '&':  // 0x31 ~ 0x26
		case '\'': case '(': case ')': case '*': case '+': case ',': // 0x27 ~ 0x2C
		case '-': case '.': case '/':								 // 0x2D ~ 0x2F
		case ':': case ';': case '<': case '=': case '>': case '?': case '@':  // 0x3A ~ 0x40
		case '[': case '\\': case ']': case '^': case '_': case '`':		   // 0x5B ~ 0x60
		case '{': case '|': case '}': case '~': {                     // 0x7B ~ 0x7E
			return true;
		}
		default:{
			return false;
		}
		}
	}
	
	/**
	 * @reference [Java：判断中英文符号、标点](http://blog.csdn.net/ztf312/article/details/54310542)
	 * @param ch
	 * @return
	 */
	public static boolean isChinesePunctuation(char ch) {
       Character.UnicodeBlock ub = Character.UnicodeBlock.of(ch);
       if (ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
               || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION  
               || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS  
               || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_FORMS
               || ub == Character.UnicodeBlock.VERTICAL_FORMS) {
           return true;
       } else {
           return false;
       }
	}
	
	public static boolean isAlpha(char ch) {
		return ('a' <= ch && ch < 'z') || ('A' <= ch && ch <= 'Z');
	}
	
	public static boolean isAlphaOrDigit(char ch) {
		return ('0' <= ch && ch <= '9') || isAlpha(ch);
	}
}
