package com.donespeak.visofh.extractor;

/**
 * Created by DoneSpeak on 2017/6/10.
 */
public enum TextType {
    DOC,
    DOCX,
    PPT,
    PPTX,
    XLS,
    XLT,
    XLSX,
    TXT,
    PDF,
    NONE
}
