package com.donespeak.visofh.extractor;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class HttpAssistant {

    // 利用 httpClient 获取 url 指向的输入流
    public static InputStream getInputStreamFromURLWithClient(URL url) throws IOException{
        String urlStr = url.toString();

        CloseableHttpClient httpclient = HttpClients.createDefault();

        //设置请求和传输超时时间
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(5000)
                .setConnectTimeout(5000)
                .build();
        HttpGet httpget = new HttpGet(urlStr);
        httpget.setConfig(requestConfig);
        //执行 get 请求
        CloseableHttpResponse response = httpclient.execute(httpget);
        HttpEntity entity = response.getEntity();
        return entity.getContent();
    }

//    利用 URLConnection 获取 url 指向的输入流
    public static InputStream getInputStreamFromURL(URL url) throws IOException {
        //返回一个 URLConnection 对象，它表示到 URL 所引用的远程对象的连接。
        URLConnection uc = url.openConnection();
        //打开的连接读取的输入流。
        return uc.getInputStream();
    }
}
