package com.donespeak.visofh.extractor;
/**
 * Created by DoneSpeak on 2017/6/9.
 */
public class ExtractorFactory {
    public static Extractor getExtracter(TextType textType){
        Extractor textExtracter = null;
        switch(textType){
            case TXT:{
                textExtracter = new TXTExtractor();
                break;
            }
            case DOCX:
            case DOC:{
                textExtracter = new DocExtracter();
                break;
            }
            case XLS:
            case XLSX:
            case XLT:{
                textExtracter = new ELExtractor();
                break;
            }
            case PDF:{
                textExtracter = new PDFExtractor();
                break;
            }
            case PPT:
            case PPTX: {
                textExtracter = new PPTExtractor();
                break;
            }
            default:{
                textExtracter = null;
            }
        }
        return textExtracter;
    }
}
