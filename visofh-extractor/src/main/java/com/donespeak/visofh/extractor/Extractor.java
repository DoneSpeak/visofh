package com.donespeak.visofh.extractor;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * extract all text from the file
 * Created by DoneSpeak on 2017/6/9.
 */
public abstract interface Extractor {
	
    /**
     * extract text from the specify file
     * @param file
     * @return
     */
    String extractText(File file) throws Exception;

    /**
     * extract text from the file pointed by the url
     * @param url an url point 
     * @return
     */
    String extractText(URL url) throws Exception;

    /**
     * extract all text from the <code>srcFile</code> to <code>destFile</code>.
     * @param srcFile
     * @param destFile
     */
    void extractText2File(File srcFile, File destFile) throws Exception;

    /**
     * extract all text from the file pointed by the <code>url</code> to the <code>destFile</code>.
     * @param url
     * @param destFile
     */
    void extractText2File(URL url, File destFile) throws IOException;
}
