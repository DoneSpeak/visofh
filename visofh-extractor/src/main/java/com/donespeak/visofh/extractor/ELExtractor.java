package com.donespeak.visofh.extractor;

import org.apache.poi.hssf.OldExcelFormatException;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hssf.extractor.OldExcelExtractor;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.extractor.XSSFExcelExtractor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.net.URL;

/**
 * Created by DoneSpeak on 2017/6/9.
 * 【参考】
 *  !!![Text Extraction](http://poi.apache.org/text-extraction.html)
 * [JExcelApi JavaDoc](http://jexcelapi.sourceforge.net/resources/javadocs/index.html)
 * [JExcelApi 2.6.10 JavaDoc](http://jexcelapi.sourceforge.net/resources/javadocs/2_6_10/docs/index.html)
 * [操作excel的jexcelapi-2.6.12](https://sourceforge.net/projects/jexcelapi/files/jexcelapi/2.6.12/)
 * [xmlbeans-2.6.0.jar](http://www.java2s.com/Code/Jar/x/Downloadxmlbeans260jar.htm)
 * [dom4j-1.6.1-hudson-1.jar](http://www.java2s.com/Code/Jar/d/Downloaddom4j161hudson1jar.htm)
 * [ Java通过poi读取word，excel，ppt文件中文本（excel，ppt部分）](http://blog.csdn.net/RunningTerry/article/details/47105767)
 * 【JAR】
 * 1. jxl.jar
 */
public class ELExtractor implements Extractor {
    /**
     *
     * @param file
     * @return
     * @throws IOException
     * 2007+的excel需要使用 XSSF 而不是 HSSF
     */
    private TextType textType = TextType.XLS;
    @Override
    public String extractText(File file) throws Exception {
        textType = FileAssistant.getOfficeType(file.getName());
        FileInputStream fis = null;
        try{
           fis = new FileInputStream(file);
           return output(fis);
        }catch (OldExcelFormatException e){
            e.printStackTrace();
            fis = new FileInputStream(file);
            return extractWithOldExcelExtractor(fis);
        }finally {
            if(fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public String extractText(URL url) throws IOException {
        textType = FileAssistant.getOfficeType(url.getPath());
        InputStream in = null;
        try{
            in = HttpAssistant.getInputStreamFromURLWithClient(url);
            String text = output(in);
            return text;
        }catch (OldExcelFormatException e){
            e.printStackTrace();
            in = HttpAssistant.getInputStreamFromURLWithClient( new URL(url.toString()));
            return extractWithOldExcelExtractor(in);
        }finally {
            if(in != null){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void extractText2File(File srcFile, File distFile) throws Exception {
        textType = FileAssistant.getOfficeType(srcFile.getName());
        String text = extractText(srcFile);
        FileAssistant.string2File(text, distFile);
    }

    @Override
    public void extractText2File(URL url, File distFile) throws IOException {
        textType = FileAssistant.getOfficeType(url.getPath());
        String text = extractText(url);
        FileAssistant.string2File(text, distFile);
    }

    public String output(InputStream in) throws IOException{
//        [JAR] commons-collections4-4.1.jar
//        [JAR] poi-ooxml-schemas-3.16.jar
        XSSFExcelExtractor xssfExtractor = null;
        ExcelExtractor excelExtractor = null;
        try {
            if (textType == TextType.XLSX) {
//            >= 2007 版本
                xssfExtractor = new XSSFExcelExtractor(new XSSFWorkbook(in));
                return xssfExtractor.getText();
            } else {
//            97-2003版本
                HSSFWorkbook wb = new HSSFWorkbook(in);
                excelExtractor = new ExcelExtractor(wb);
                return excelExtractor.getText();
            }
        }finally {
            if(xssfExtractor != null){
                try {
                    xssfExtractor.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(excelExtractor != null){
                try {
                    excelExtractor.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String extractWithOldExcelExtractor(InputStream in) throws IOException {
        OldExcelExtractor extractor = new OldExcelExtractor(in);
        return extractor.getText();
    }
}
